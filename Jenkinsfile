pipeline {
  agent any

  environment {
    TOMCAT_DEPLOYMENT_DIR = "/opt/tomcat/apache-tomcat-9.0.55/webapps/"
    WEB_APP_DEPLOYEMNT_WAR = "sample.war"
    WEB_APP_DEPLOYMENT_REPO = "https://jenisakthi@bitbucket.org/jenisakthi/samplepythonjeni.git"
    WEB_APP_DEPLOYMENT_BRANCH = "master"
    TOMCAT_SERVER_USER_NAME = "root"
    TOMCAT_SERVER_PASSWORD = "jenkins"
    TOMCAT_SERVER_HOSTNAME = "192.168.0.104"
    TOMCAT_SERVER_SSH_PORT = "22"
    TOMCAT_BKP_ROOT_FOLDER = '/home/jenkins/deployment_bkp'
    GIT_DEPLOYMENT_REPO_CRED_ID = 'bitbucket-creds'
    JENIKINS_WORKSPACE_DIR = "/tmp/jenkins_wrk_dir"
  }

  stages {

    stage('setup work environment') {
      steps {
        script {
          ENV = '/PROD/'
          env.BUILD = sh(
            script: "date +'%d_%B_%Y_%Hhr_%MMin_%Ss'",
            encoding: "UTF-8",
            returnStdout: true
          ).trim()
          env.CURRENT_BUILD_PATH = env.JENIKINS_WORKSPACE_DIR + ENV + env.BUILD
          env.CURRENT_ARTIFACTS = env.CURRENT_BUILD_PATH + '/artifacts'
        }
      }
    }

    stage('clone & build latest deployment') {
      steps {

        dir(env.CURRENT_ARTIFACTS) {
          script {
            git credentialsId: env.GIT_DEPLOYMENT_REPO_CRED_ID, url: env.WEB_APP_DEPLOYMENT_REPO, branch: env.WEB_APP_DEPLOYMENT_BRANCH
          }
        }
      }
    }

    stage('build latest deployment war file') {
      steps {
        dir(env.CURRENT_ARTIFACTS) {
          script {
            sh(
              script: "jar -cvf $env.CURRENT_BUILD_PATH/$env.WEB_APP_DEPLOYEMNT_WAR ./index.html"
            )
          }
        }
      }
    }

    stage('setup current backup environment') {
      steps {
        script {

          if (sh(
              script: "/usr/bin/sshpass -p '$TOMCAT_SERVER_PASSWORD' ssh -o StrictHostKeyChecking=no '$TOMCAT_SERVER_USER_NAME@$TOMCAT_SERVER_HOSTNAME' -p 22 -tt 'ls $env.TOMCAT_BKP_ROOT_FOLDER' ",
              returnStatus: true
            ).equals(0)) {
            if (sh(
                script: "/usr/bin/sshpass -p '$TOMCAT_SERVER_PASSWORD' ssh -o StrictHostKeyChecking=no '$TOMCAT_SERVER_USER_NAME@$TOMCAT_SERVER_HOSTNAME' -p 22 -tt 'mkdir $env.TOMCAT_BKP_ROOT_FOLDER/$env.BUILD' ",
                returnStatus: true
              ).equals(0)) {
              echo "Created backup directory: $env.TOMCAT_BKP_ROOT_FOLDER/$env.BUILD"
            } else {
              echo 'Failed to create current backup directory'
              sh "exit 1"
            }

          } else {

            if (sh(
                script: "/usr/bin/sshpass -p '$TOMCAT_SERVER_PASSWORD' ssh -o StrictHostKeyChecking=no '$TOMCAT_SERVER_USER_NAME@$TOMCAT_SERVER_HOSTNAME' -p 22 -tt 'mkdir $env.TOMCAT_BKP_ROOT_FOLDER' ",
                returnStatus: true
              ).equals(0)) {
              if (sh(
                  script: "/usr/bin/sshpass -p '$TOMCAT_SERVER_PASSWORD' ssh -o StrictHostKeyChecking=no '$TOMCAT_SERVER_USER_NAME@$TOMCAT_SERVER_HOSTNAME' -p 22 -tt 'mkdir $env.TOMCAT_BKP_ROOT_FOLDER/$env.BUILD' ",
                  returnStatus: true
                ).equals(0)) {
                echo "Created backup directory: $env.TOMCAT_BKP_ROOT_FOLDER/$env.BUILD"
              } else {
                echo 'Failed to create current backup directory'
                sh "exit 1"
              }
            } else {
              echo 'Failed to create backup root directory'
              sh "exit 1"
            }
          }
        }
      }
    }

    stage('create backup') {
      steps {
        script {
          WEBAPPS = sh(
            script: "/usr/bin/sshpass -p '$TOMCAT_SERVER_PASSWORD' ssh -o StrictHostKeyChecking=no '$TOMCAT_SERVER_USER_NAME@$TOMCAT_SERVER_HOSTNAME' -p 22 -tt 'find $TOMCAT_DEPLOYMENT_DIR -name $WEB_APP_DEPLOYEMNT_WAR'",
            encoding: "UTF-8",
            returnStdout: true
          ).trim()
          if (WEBAPPS.equals(TOMCAT_DEPLOYMENT_DIR + WEB_APP_DEPLOYEMNT_WAR)) {
            echo "Deployment not found to backup"
            if (sh(
                script: "/usr/bin/sshpass -p '$TOMCAT_SERVER_PASSWORD' ssh -o StrictHostKeyChecking=no '$TOMCAT_SERVER_USER_NAME@$TOMCAT_SERVER_HOSTNAME' -p 22 -tt 'cp $TOMCAT_DEPLOYMENT_DIR$WEB_APP_DEPLOYEMNT_WAR   $env.TOMCAT_BKP_ROOT_FOLDER/$env.BUILD/' ",
                returnStatus: true
              ).equals(0)) {
              echo 'Deployment backup is successful'
            } else {
              echo 'Failed to backup deployment'
              sh "exit 1"
            }
          } else {
            echo "Old deployment not found to backup"
          }
        }
      }
    }

    stage('prepare deployment') {
      steps {
        script {
          if (sh(
              script: "/usr/bin/sshpass -p '$TOMCAT_SERVER_PASSWORD' ssh -o StrictHostKeyChecking=no '$TOMCAT_SERVER_USER_NAME@$TOMCAT_SERVER_HOSTNAME' -p 22 -tt 'systemctl stop tomcat' ",
              returnStatus: true
            ).equals(0)) {
            echo 'Tomcat is stopped'
          } else {
            echo 'Cannot stop tomcat service for deployment'
            sh "exit 1"
          }
        }
      }
    }
    stage('Deploy latest deployment') {
      steps {
        script {
          if (sh(
              script: "/usr/bin/sshpass -p '$TOMCAT_SERVER_PASSWORD' scp -P $TOMCAT_SERVER_SSH_PORT $env.CURRENT_BUILD_PATH/$env.WEB_APP_DEPLOYEMNT_WAR '$TOMCAT_SERVER_USER_NAME@$TOMCAT_SERVER_HOSTNAME:$TOMCAT_DEPLOYMENT_DIR/$WEB_APP_DEPLOYEMNT_WAR'",
              returnStatus: true
            ).equals(0)) {
            echo 'Latest war is deployed to tomcat successfully'
          } else {
            echo 'Failed latest war deployment to tomcat'
            sh "exit 1"
          }
        }
      }
    }

    stage('Restart tomcat server') {
      steps {
        script {
          if (sh(
              script: "/usr/bin/sshpass -p '$TOMCAT_SERVER_PASSWORD' ssh -o StrictHostKeyChecking=no '$TOMCAT_SERVER_USER_NAME@$TOMCAT_SERVER_HOSTNAME' -p 22 -tt 'systemctl start tomcat' ",
              returnStatus: true
            ).equals(0)) {
            echo 'Tomcat server deployment is successful'
          } else {
            echo 'Failed to start tomcat after latest deployment'
            sh "exit 1"
          }
        }
      }
    }
  }
}